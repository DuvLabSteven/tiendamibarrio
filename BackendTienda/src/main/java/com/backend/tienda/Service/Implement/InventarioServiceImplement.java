/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.backend.tienda.Service.Implement;


import com.backend.tienda.Dao.InventarioDao;
import com.backend.tienda.Models.Inventario;
import com.backend.tienda.Service.InventarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author duvan
 */
@Service
public class InventarioServiceImplement implements InventarioService {
@Autowired
    private InventarioDao inventarioDao;


    @Override
    @Transactional (readOnly=false)
    public Inventario save (Inventario inventario){
        return inventarioDao.save(inventario);

    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        inventarioDao.deleteById(id);
    }
    @Override
    @Transactional(readOnly=true)
    public Inventario findById(Integer id){
        return inventarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Inventario> findByAll(){
        return (List<Inventario>) inventarioDao.findAll();
    }
    
}


