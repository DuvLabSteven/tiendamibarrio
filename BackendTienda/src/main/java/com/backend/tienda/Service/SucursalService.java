/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.backend.tienda.Service;

import com.backend.tienda.Models.Sucursal;
import java.util.List;

/**
 *
 * @author duvan
 */
public interface SucursalService {
  public Sucursal save(Sucursal sucursal);
  public void delete(Integer id);
  public Sucursal findById(Integer id);
  public List<Sucursal> findByAll();
  
    
}
