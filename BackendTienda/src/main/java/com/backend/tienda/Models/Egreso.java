/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.backend.tienda.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table
@Entity(name = "egreso")
public class Egreso implements Serializable {
    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idEgreso")
    private int id_egreso;

    @Column(name="nombreGasto")
    private String nombre_gasto;
    
    @Column(name="valorGasto")
    private int valor_gasto;

   

    @Column(name="idSucursal")
    private int id_sucursal;

     public Egreso(int idEgreso, String nombreGasto, int valorGasto, int idSucursal) {
        this.id_egreso = idEgreso;
        this.nombre_gasto = nombreGasto;
        this.valor_gasto = valorGasto;
        this.id_sucursal = idSucursal;
    }

    public Egreso() {
    }

    public int getIdEgreso() {
        return id_egreso;
    }

    public void setIdEgreso(int idEgreso) {
        this.id_egreso = idEgreso;
    }

    public String getNombreGasto() {
        return nombre_gasto;
    }

    public void setNombreGasto(String nombreGasto) {
        this.nombre_gasto = nombreGasto;
    }

    public int getValorGasto() {
        return valor_gasto;
    }

    public void setValorGasto(int valorGasto) {
        this.valor_gasto = valorGasto;
    }

    public int getIdSucursal() {
        return id_sucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.id_sucursal = idSucursal;
    }

   
}



