/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.backend.tienda.Models;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Table
@Entity(name="sucursal")
public class Sucursal implements Serializable {
    @Id 
@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_sucursal")
    private int id_sucursal;

    @Column(name="ciudad")
    private String ciudad;
    
    @Column(name="direccion")
    private String direccion;

    @ManyToOne
    @JoinColumn(name="id_administrador")
    private Administrador administrador;

public Sucursal(int idSucursal, String ciudad, String direccion, Administrador administrador){
       super(); // Hereda de la clase principal
       this.id_sucursal = idSucursal;
       this.ciudad = ciudad;
       this.direccion = direccion;
       this.administrador = administrador;
}

public Sucursal(){
}
public int getIdSucursal(){
      return id_sucursal;
}
public void setIdSucursal(int idSucursal){
      this.id_sucursal = idSucursal;
}

public String getCiudad(){
      return ciudad;
}
public void setCiudad(String ciudad){
      this.ciudad = ciudad;
}

public String getDireccion(){
    return direccion;
}
public void setDireccion(String direccion){
    this.direccion = direccion;
}

public Administrador getIdAdministrador(){
     return administrador;
}

public void setIdAdministrador(Administrador administrador){
     this.administrador = administrador;
}
}