
package com.backend.tienda.Dao;
import com.backend.tienda.Models.Administrador;
import org.springframework.data.repository.CrudRepository;

public interface AdministradorDao extends CrudRepository<Administrador, Integer>{
    
}
