/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.backend.tienda.Dao;

import com.backend.tienda.Models.Sucursal;
import org.springframework.data.repository.CrudRepository;


public interface SucursalDao extends CrudRepository<Sucursal, Integer>{
    
}
