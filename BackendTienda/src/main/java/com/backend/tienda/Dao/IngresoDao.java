/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.backend.tienda.Dao;

import com.backend.tienda.Models.Ingreso;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author duvan
 */
public interface IngresoDao extends CrudRepository<Ingreso, Integer>{
    
}
