/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.backend.tienda.Dao;

import com.backend.tienda.Models.Egreso;
import org.springframework.data.repository.CrudRepository;

public interface EgresoDao extends CrudRepository<Egreso, Integer>{
    
}
