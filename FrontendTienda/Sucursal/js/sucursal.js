const url = "http://localhost:8080/sucursal/";
const url1 = "http://localhost:8080/sucursal/list";

const contenedor = document.querySelector("tbody");
let resultados = "";

const modalSucursal = new bootstrap.Modal(
  document.getElementById("modalSucursal")
);
const formSucursal = document.querySelector("form");
const idSucursal = document.getElementById("id");
const idAdministrador = document.getElementById("idAdministrador");
const ciudadSucursal = document.getElementById("ciudad");
const direccionSucursal = document.getElementById("direccion");
const btnCrear = document.getElementById("btnCrear");
const btnCerrar = document.getElementById("btn-close");
const btnX = document.getElementById("btn-x");

let opcion = "";

btnCrear.addEventListener("click", () => {
  idSucursal.value = "";
  ciudadSucursal.value = "";
  direccionSucursal.value = "";
  idAdministrador.value = "";
  idSucursal.disabled = true;
  idAdministrador.disabled =false;
  modalSucursal.show();
  opcion = "crear";
});
btnCerrar.addEventListener("click", () => {
  modalSucursal.hide();
});
btnX.addEventListener("click", () => {
  modalSucursal.hide();
});

const mostrar = (Sucursal) => {
  Sucursal.forEach((Sucursal) => {
    resultados += `<tr><td >${Sucursal.idSucursal}</td><td>${Sucursal.ciudad}</td><td >${Sucursal.direccion}</td> <td >${Sucursal.idAdministrador.idAdministrador}</td>
<td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td></tr>`;
  });
  contenedor.innerHTML = resultados;
};

fetch(url1)
  .then((response) => response.json())
  .then((data) => mostrar(data))
  .catch((error) => console.log(error));

const on = (element, event, selector, handler) => {
  element.addEventListener(event, (e) => {
    if (e.target.closest(selector)) handler(e);
  });
};

on(document, "click", ".btnBorrar", (e) => {
  const fila = e.target.parentNode.parentNode;
  const id = fila.firstElementChild.innerHTML;
  console.log(id);

  alertify.confirm(
    "Desea eliminar la Sucursal " + id,
    function () {
      fetch(url + id, {
        method: "DELETE",
      }).then(() => location.reload());
    },
    function () {
      alertify.error("Cancel");
    }
  );
});

let idForm = 0;
on(document, "click", ".btnEditar", (e) => {
  const fila = e.target.parentNode.parentNode;
  idForm = fila.children[0].innerHTML;
  const ciudad = fila.children[1].innerHTML;
  const direccion = fila.children[2].innerHTML;
  const administrador = fila.children[3].innerHTML;
  idSucursal.value = idForm;
  
  ciudadSucursal.value = ciudad;
  direccionSucursal.value = direccion;
  idAdministrador.value = administrador;
  idSucursal.disabled = true;
  idAdministrador.disabled =true;
  opcion = "editar";
  modalSucursal.show();
});
formSucursal.addEventListener("submit", (e) => {
  e.preventDefault();
  if (opcion == "crear") {
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ciudad: ciudadSucursal.value,
        direccion: direccionSucursal.value,
        idAdministrador:{
          idAdministrador: idAdministrador.value
        }
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        const nuevoSucursal = [];
        nuevoSucursal.push(data);
        mostrar(nuevoSucursal);
      });
  }
  if (opcion == "editar") {
    fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        
        ciudad: ciudadSucursal.value,
        direccion: direccionSucursal.value,
        idAdministrador:{
          idAdministrador: idAdministrador.value
        }
      }),
    }).then((response) => location.reload());
  }
  modalSucursal.hide();
});
