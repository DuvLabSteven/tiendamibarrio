const url = "http://localhost:8080/administrador/";
const url1 = "http://localhost:8080/administrador/list";

const contenedor = document.querySelector('tbody');
let resultados = '';

const modalAdministrador = new
    bootstrap.Modal(document.getElementById('modalAdministrador'));
const formAdministrador = document.querySelector('form');
const idAdministrador = document.getElementById('id');
const nombreAdministrador = document.getElementById('nombre');
const emailAdministrador = document.getElementById('email');
const claveAdministrador = document.getElementById('clave');
const btnCrear = document.getElementById('btnCrear');
const btnCerrar = document.getElementById('btn-close');
const btnX = document.getElementById('btn-x');

let opcion = '';

btnCrear.addEventListener('click', () => {
    idAdministrador.value = '';
    nombreAdministrador.value = '';
    emailAdministrador.value = "";
    claveAdministrador.value = "";
    idAdministrador.disabled = false;
    modalAdministrador.show();
    opcion = 'crear'
})
btnCerrar.addEventListener('click', () => {
    modalAdministrador.hide();
})
btnX.addEventListener('click', () => {
    modalAdministrador.hide();
})

const mostrar = (Administrador) => {
Administrador.forEach(Administrador => {
        resultados += `<tr><td >${Administrador.idAdministrador}</td><td >${Administrador.nombreAdministrador}</td><td >${Administrador.clave}</td><td >${Administrador.email}</td>
<td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td></tr>`});
    contenedor.innerHTML = resultados;
}

fetch(url1)
    .then(response => response.json())
    .then(data => mostrar(data))
    .catch(error => console.log(error));


const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector)) handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode;
    const id = fila.firstElementChild.innerHTML;
    console.log(id)

    alertify.confirm("Desea eliminar el Administrador " + id, function () {
        fetch(url + id, {
            method: 'DELETE'
        })
            .then(() => location.reload())
    },
        function () {
            alertify.error('Cancel')
        });
})

let idForm = 0;
on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode;
    idForm = fila.children[0].innerHTML;
    const nombre = fila.children[1].innerHTML;
    const clave = fila.children[2].innerHTML;
    const email = fila.children[3].innerHTML;
    idAdministrador.value = idForm;
    nombreAdministrador.value = nombre;
    claveAdministrador.value = clave;
    emailAdministrador.value = email;
    idAdministrador.disabled = true;
    opcion = 'editar';
    modalAdministrador.show()
});
formAdministrador.addEventListener('submit', (e) => {
    e.preventDefault()
    if (opcion == 'crear') {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idAdministrador: idAdministrador.value,
                nombreAdministrador: nombreAdministrador.value,
                clave: claveAdministrador.value,
                email: emailAdministrador.value,
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevoAdministrador = []
                nuevoAdministrador.push(data)
                mostrar(nuevoAdministrador)
            })
    }
    if (opcion == 'editar') {
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idAdministrador: idAdministrador.value,
                nombreAdministrador: nombreAdministrador.value,
                clave: claveAdministrador.value,
                email: emailAdministrador.value,
            })
        })
            .then(response => location.reload())
    }
    modalAdministrador.hide();

})

