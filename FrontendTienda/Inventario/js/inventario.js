const url = "http://localhost:8080/inventario/";
const url1 = "http://localhost:8080/inventario/list";

const contenedor = document.querySelector('tbody');
let resultados = '';

const modalInventario = new
    bootstrap.Modal(document.getElementById('modalInventario'));
const formInventario = document.querySelector('form');
const idInventario = document.getElementById('id');
const nombreInventario = document.getElementById('nombre');
const cantidadProducto = document.getElementById('cantidadProducto');
const valorProducto = document.getElementById('valorProducto');
const btnCrear = document.getElementById('btnCrear');
const btnCerrar = document.getElementById('btn-close');
const btnX = document.getElementById('btn-x');

let opcion = '';

btnCrear.addEventListener('click', () => {
    idInventario.value = '';
    nombreInventario.value = '';
    cantidadProducto.value = "";
    valorProducto.value = "";
    modalInventario.show();
    opcion = 'crear'
})
btnCerrar.addEventListener('click', () => {
    modalInventario.hide();
})
btnX.addEventListener('click', () => {
    modalInventario.hide();
})

const mostrar = (Inventario) => {
Inventario.forEach(Inventario => {
        resultados += `<tr><td >${Inventario.idProducto}</td><td >${Inventario.nombreProducto}</td><td >${Inventario.cantidadProducto}</td><td >${Inventario.valorProducto}</td>
<td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td></tr>`});
    contenedor.innerHTML = resultados;
}

fetch(url1)
    .then(response => response.json())
    .then(data => mostrar(data))
    .catch(error => console.log(error));


const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector)) handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode;
    const id = fila.firstElementChild.innerHTML;
    console.log(id)

    alertify.confirm("Desea eliminar el Inventario " + id, function () {
        fetch(url + id, {
            method: 'DELETE'
        })
            .then(() => location.reload())
    },
        function () {
            alertify.error('Cancel')
        });
})

let idForm = 0;
on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode;
    idForm = fila.children[0].innerHTML;
    const nombre = fila.children[1].innerHTML;
    const cantidad = fila.children[2].innerHTML;
    const valor = fila.children[3].innerHTML;
    idInventario.value = idForm;
    nombreInventario.value = nombre;
    cantidadProducto.value = cantidad;
    valorProducto.value = valor;
    opcion = 'editar';
    modalInventario.show()
});
formInventario.addEventListener('submit', (e) => {
    e.preventDefault()
    if (opcion == 'crear') {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idInventario: idInventario.value,
                nombreInventario: nombreInventario.value,
                cantidadProducto: cantidadProducto.value,
                valorProducto: valorProducto.value,
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevoInventario = []
                nuevoInventario.push(data)
                mostrar(nuevoInventario)
            })
    }
    if (opcion == 'editar') {
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idInventario: idInventario.value,
                nombreInventario: nombreInventario.value,
                cantidadProducto: cantidadProducto.value,
                valorProducto: valorProducto.value,
            })
        })
            .then(response => location.reload())
    }
    modalInventario.hide();

})

