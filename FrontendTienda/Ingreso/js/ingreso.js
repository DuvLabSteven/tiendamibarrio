const url = "http://localhost:8080/ingreso/";
const url1 = "http://localhost:8080/ingreso/list";

const contenedor = document.querySelector('tbody');
let resultados = '';

const modalIngreso = new
    bootstrap.Modal(document.getElementById('modalIngreso'));
const formIngreso = document.querySelector('form');
const idIngreso = document.getElementById('id');
const cantidadProducto = document.getElementById('cantidad');
const valorVenta = document.getElementById('valor');
const idSucursal = document.getElementById('id_sucursal');
const idProducto = document.getElementById('id_producto');
const btnCrear = document.getElementById('btnCrear');
const btnCerrar = document.getElementById('btn-close');
const btnX = document.getElementById('btn-x');

let opcion = '';

btnCrear.addEventListener('click', () => {
    idIngreso.value = '';
    cantidadProducto.value = '';
    valorVenta.value = "";
    idSucursal.disabled = false;
    modalIngreso.show();
    opcion = 'crear'
})
btnCerrar.addEventListener('click', () => {
    modalIngreso.hide();
})
btnX.addEventListener('click', () => {
    modalIngreso.hide();
})

const mostrar = (Ingreso) => {
Ingreso.forEach(Ingreso => {
        resultados += `<tr><td >${Ingreso.idVenta}</td><td >${Ingreso.cantidadProducto}</td><td >${Ingreso.valorVenta}</td><td >${Ingreso.idSucursal}</td><td >${Ingreso.idProducto}</td>
<td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td></tr>`});
    contenedor.innerHTML = resultados;
}

fetch(url1)
    .then(response => response.json())
    .then(data => mostrar(data))
    .catch(error => console.log(error));


const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector)) handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode;
    const id = fila.firstElementChild.innerHTML;
    console.log(id)

    alertify.confirm("Desea eliminar el Ingreso " + id, function () {
        fetch(url + id, {
            method: 'DELETE'
        })
            .then(() => location.reload())
    },
        function () {
            alertify.error('Cancel')
        });
})

let idForm = 0;
on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode;
    idForm = fila.children[0].innerHTML;
    const cantidad = fila.children[1].innerHTML;
    const valor = fila.children[2].innerHTML;
    const sucursal = fila.children[3].innerHTML;
    const producto = fila.children[4].innerHTML;
    idIngreso.value = idForm;
    cantidadProducto.value = cantidad;
    valorVenta.value = valor;
    idSucursal.value = sucursal;
    idProducto.value = producto;
    idSucursal.disabled = true;
    idProducto.disabled = true;
   
    opcion = 'editar';
    modalIngreso.show()
});
formIngreso.addEventListener('submit', (e) => {
    e.preventDefault()
    if (opcion == 'crear') {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idIngreso: idIngreso.value,
                cantidadProducto: cantidadProducto.value,
                valorVenta: valorVenta.value,
                idSucursal: idSucursal.value,
                idProducto: idProducto.value,
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevoIngreso = []
                nuevoIngreso.push(data)
                mostrar(nuevoIngreso)
            })
    }
    if (opcion == 'editar') {
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idIngreso: idIngreso.value,
                cantidadProducto: cantidadProducto.value,
                valorVenta: valorVenta.value,
                idSucursal: idSucursal.value,
                idProducto: idProducto.value,
            })
        })
            .then(response => location.reload())
    }
    modalIngreso.hide();

})

