const url = "http://localhost:8080/egreso/";
const url1 = "http://localhost:8080/egreso/list";

const contenedor = document.querySelector('tbody');
let resultados = '';

const modalEgreso = new
    bootstrap.Modal(document.getElementById('modalEgreso'));
const formEgreso = document.querySelector('form');
const idEgreso = document.getElementById('id');
const idSucursal = document.getElementById('idS');
const nombreGasto = document.getElementById('nombre');
const valorGasto = document.getElementById('valorgasto');

const btnCrear = document.getElementById('btnCrear');
const btnCerrar = document.getElementById('btn-close');
const btnX = document.getElementById('btn-x');

let opcion = '';

btnCrear.addEventListener('click', () => {
    idEgreso.value = '';
    nombreGasto.value = '';
    valorGasto.value = "";
    idSucursal.value = "";
    idSucursal.disabled = false;
    modalEgreso.show();
    opcion = 'crear'
})
btnCerrar.addEventListener('click', () => {
    modalEgreso.hide();
})
btnX.addEventListener('click', () => {
    modalEgreso.hide();
})

const mostrar = (Egreso) => {
Egreso.forEach(Egreso => {
        resultados += `<tr><td >${Egreso.idEgreso}</td><td >${Egreso.nombreGasto}</td><td >${Egreso.valorGasto}</td><td >${Egreso.idSucursal}</td>
<td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td></tr>`});
    contenedor.innerHTML = resultados;
}

fetch(url1)
    .then(response => response.json())
    .then(data => mostrar(data))
    .catch(error => console.log(error));


const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector)) handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode;
    const id = fila.firstElementChild.innerHTML;
    console.log(id)

    alertify.confirm("Desea eliminar el Egreso " + id, function () {
        fetch(url + id, {
            method: 'DELETE'
        })
            .then(() => location.reload())
    },
        function () {
            alertify.error('Cancel')
        });
})

let idForm = 0;
on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode;
    idForm = fila.children[0].innerHTML;
    const nombre = fila.children[1].innerHTML;
    const  valor= fila.children[2].innerHTML;
    const sucursal = fila.children[3].innerHTML;
    idEgreso.value = idForm;
    nombreGasto.value = nombre;
    valorGasto.value = valor;
    idSucursal.value = sucursal;
    idSucursal.disabled =true;
    opcion = 'editar';
    modalEgreso.show()
});
formEgreso.addEventListener('submit', (e) => {
    e.preventDefault()
    if (opcion == 'crear') {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idEgreso: idEgreso.value,
                nombreGasto: nombreGasto.value,
                valorGasto: valorGasto.value,
                idSucursal: idSucursal.value,
                

            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevoEgreso = []
                nuevoEgreso.push(data)
                mostrar(nuevoEgreso)
            })
    }
    if (opcion == 'editar') {
        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idEgreso: idEgreso.value,
                nombreGasto: nombreGasto.value,
                valorGasto: valorGasto.value,
                idSucursal: idSucursal.value,
            })
        })
            .then(response => location.reload())
    }
    modalEgreso.hide();

})

